INSERT INTO contractor (name) VALUES ('Toro Impex');
INSERT INTO contractor (name) VALUES ('MI-TU-HAZ');

--INSERT INTO customer (name) VALUES ('ABC');
--INSERT INTO customer (name) VALUES ('CBA');
--INSERT INTO customer (name) VALUES ('Carrefour');

INSERT INTO product (name, importer_id, quantity) VALUES ('apple', 1, 15);
INSERT INTO product (name, importer_id, quantity) VALUES ('pear', 2, 42);
INSERT INTO product (name, importer_id, quantity) VALUES ('cherry', 2, 20);

INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (1, '2023-01-01', 15, false);
INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (1, '2023-01-02', 16, false);
INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (2, '2023-01-03', 17, false);
INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (1, '2023-01-04', 10, false);
INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (2, '2023-01-04', 1, false);
INSERT INTO orders (product_id, ordered_on, quantity, completed) VALUES (2, '2023-01-05', 21, false);
