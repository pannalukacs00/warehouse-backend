package ubb.hpc.fmcp.transactionalsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ubb.hpc.fmcp.transactionalsystem.dto.ProductDTO;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.mapper.ProductMapper;
import ubb.hpc.fmcp.transactionalsystem.service.ProductService;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @CrossOrigin
    @GetMapping
    public List<ProductDTO> getAllProducts() {
        return ProductMapper.modelsToDtos(productService.getAllProducts());
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ProductDTO getProductById(@PathVariable Long id) {
        if (productService.getProductById(id).isPresent()) {
            return ProductMapper.modelToDto(productService.getProductById(id).get());
        }
        throw new EntityNotFoundException(id);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public ProductDTO modifyProduct(@PathVariable Long id, @RequestBody ProductDTO productDTO) {
        return ProductMapper.modelToDto(productService.modifyProduct(id, productDTO));
    }
}
