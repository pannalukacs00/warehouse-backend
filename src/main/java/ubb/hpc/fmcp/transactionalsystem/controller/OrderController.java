package ubb.hpc.fmcp.transactionalsystem.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ubb.hpc.fmcp.transactionalsystem.dto.OrderDTO;
import ubb.hpc.fmcp.transactionalsystem.mapper.OrderMapper;
import ubb.hpc.fmcp.transactionalsystem.service.OrderService;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @CrossOrigin
    @GetMapping
    public List<OrderDTO> getAllOrders() {
        return OrderMapper.modelsToDtos(orderService.getAllOrders());
    }

    @CrossOrigin
    @PostMapping
    public OrderDTO placeNewOrder(@RequestBody OrderDTO orderDTO) {
        return OrderMapper.modelToDto(orderService.placeNewOrder(orderDTO));
    }

    @CrossOrigin
    @SneakyThrows
    @PostMapping("/complete")
    public Boolean completeOrder(@RequestBody OrderDTO orderDTO) {
        return orderService.completeOrder(orderDTO.getId());
    }
}
