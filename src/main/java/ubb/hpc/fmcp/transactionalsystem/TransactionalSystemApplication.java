package ubb.hpc.fmcp.transactionalsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ubb.hpc.fmcp.transactionalsystem.repository.ProductRepository;

@SpringBootApplication
@EnableJpaRepositories
public class TransactionalSystemApplication {
    @Autowired
    ProductRepository productRepository;

    public static void main(String[] args) {

        SpringApplication.run(TransactionalSystemApplication.class, args);
    }

}
