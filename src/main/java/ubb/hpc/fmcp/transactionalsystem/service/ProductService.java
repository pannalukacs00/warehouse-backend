package ubb.hpc.fmcp.transactionalsystem.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ubb.hpc.fmcp.transactionalsystem.dto.ProductDTO;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.model.Product;
import ubb.hpc.fmcp.transactionalsystem.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @SneakyThrows
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @SneakyThrows
    public Optional<Product> getProductById(Long id) {
        return productRepository.findById(id);
    }

    @SneakyThrows
    public Product modifyProduct(Long id, ProductDTO productDTO) {
        // select: check is product exists
        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent()) {
            Product productValue = product.get();
            productValue.setQuantity(productDTO.getQuantity());

            // update: new product
            return productRepository.save(productValue);
        }
        throw new EntityNotFoundException(id);
    }
}
