package ubb.hpc.fmcp.transactionalsystem.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ubb.hpc.fmcp.transactionalsystem.dto.OrderDTO;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.exception.InvalidRequestException;
import ubb.hpc.fmcp.transactionalsystem.mapper.OrderMapper;
import ubb.hpc.fmcp.transactionalsystem.model.Order;
import ubb.hpc.fmcp.transactionalsystem.model.Product;
import ubb.hpc.fmcp.transactionalsystem.repository.OrderRepository;
import ubb.hpc.fmcp.transactionalsystem.repository.ProductRepository;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @SneakyThrows
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @SneakyThrows
    public Order placeNewOrder(OrderDTO orderDTO) {
        // first select the requested product
        Product product = productRepository.findByName(orderDTO.getProduct()).orElseThrow(EntityNotFoundException::new);

        // insert new order
        Order order = OrderMapper.dtoToModel(orderDTO, product);
        return orderRepository.save(order);
    }

    @SneakyThrows
    public Boolean completeOrder(Long orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(EntityNotFoundException::new);

        if (Boolean.TRUE.equals(order.getCompleted())) {
            return false;
        }

        Product requestedProduct = order.getProduct();
        Product inventoryProduct = productRepository.findById(requestedProduct.getId()).orElseThrow(EntityNotFoundException::new);

        // first check if there is enough quantity in the inventory
        if (inventoryProduct.getQuantity() < order.getQuantity()) {
            throw new InvalidRequestException("Not enough resource in the inventory!");
        }

        // complete order

        // reduce quantity in inventory
        inventoryProduct.setQuantity(inventoryProduct.getQuantity() - order.getQuantity());
        productRepository.save(inventoryProduct);

        // mark order as completed
        order.setCompleted(true);
        orderRepository.save(order);

        return true;
    }
}
