package ubb.hpc.fmcp.transactionalsystem.repository;

import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.model.Contractor;
import ubb.hpc.fmcp.transactionalsystem.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class ProductRepository {
    @Autowired
    private HikariDataSource dataSource;

    @Autowired
    private ContractorRepository contractorRepository;

    public List<Product> findAll() throws SQLException {
        String sqlStatement = "SELECT * FROM Product;";
        List<Product> productList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    productList.add(resultSetToProduct(resultSet));
                }
                connection.commit();

                return productList;
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } finally {
                connection.setAutoCommit(true);
            }
        }

        return Collections.emptyList();
    }

    public Optional<Product> findById(Long id) throws SQLException {
        String sqlStatement = "SELECT * FROM Product WHERE id = ?";

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setLong(1, id);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    connection.commit();
                    return resultSetToOptionalProduct(resultSet);
                } else {
                    throw new EntityNotFoundException(id);
                }
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } catch (EntityNotFoundException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
                throw e;
            } finally {
                connection.setAutoCommit(true);
            }
        }

        return Optional.empty();
    }

    public Optional<Product> findByName(String name) throws SQLException {
        String sqlStatement = "SELECT * FROM Product WHERE name LIKE ?";

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setString(1, name);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    connection.commit();
                    return resultSetToOptionalProduct(resultSet);
                } else {
                    throw new EntityNotFoundException();
                }
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } catch (EntityNotFoundException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
                throw e;
            } finally {
                connection.setAutoCommit(true);
            }
        }
        return Optional.empty();
    }

    public Product save(Product product) throws SQLException {
        String sqlStatement;
        boolean isUpdating = false;

        if (product.getId() == null) {
            sqlStatement = "INSERT INTO product (name, quantity, importer_id) VALUES (?, ?, ?)";
        } else {
            isUpdating = true;
            sqlStatement = "UPDATE product SET name=?, quantity=?, importer_id=? WHERE id=?";
        }

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setString(1, product.getName());
                statement.setLong(2, product.getQuantity());
                statement.setLong(3, product.getImporter().getId());

                if (Boolean.TRUE.equals(isUpdating)) {
                    statement.setLong(4, product.getId());
                }

                statement.executeUpdate();
                connection.commit();

                return product;
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } finally {
                connection.setAutoCommit(true);
            }
        }
        return null;
    }

    private Optional<Product> resultSetToOptionalProduct(ResultSet resultSet) throws SQLException {
        Contractor contractor = contractorRepository.findById(resultSet.getLong(4)).get();

        Product product = new Product();
        product.setId(resultSet.getLong(1));
        product.setName(resultSet.getString(2));
        product.setQuantity(resultSet.getInt(3));
        product.setImporter(contractor);

        return Optional.ofNullable(product);
    }

    private Product resultSetToProduct(ResultSet resultSet) throws SQLException {
        Contractor contractor = contractorRepository.findById(resultSet.getLong(4)).get();

        Product product = new Product();
        product.setId(resultSet.getLong(1));
        product.setName(resultSet.getString(2));
        product.setQuantity(resultSet.getInt(3));
        product.setImporter(contractor);


        return product;
    }
}