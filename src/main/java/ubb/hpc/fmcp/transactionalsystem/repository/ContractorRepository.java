package ubb.hpc.fmcp.transactionalsystem.repository;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.model.Contractor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Slf4j
@Repository
public class ContractorRepository {
    @Autowired
    private HikariDataSource dataSource;

    public Optional<Contractor> findById(Long id) throws SQLException {
        String sqlStatement = "SELECT * FROM contractor WHERE id = ?";

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setLong(1, id);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    connection.commit();
                    return resultSetToOptionalContractor(resultSet);
                } else {
                    throw new EntityNotFoundException(id);
                }
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } catch (EntityNotFoundException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
                throw e;
            } finally {
                connection.setAutoCommit(true);
            }
            return Optional.empty();
        }
    }

    private Optional<Contractor> resultSetToOptionalContractor(ResultSet resultSet) throws SQLException {
        Contractor contractor = new Contractor();
        contractor.setId(resultSet.getLong(1));
        contractor.setName(resultSet.getString(2));

        return Optional.ofNullable(contractor);
    }
}
