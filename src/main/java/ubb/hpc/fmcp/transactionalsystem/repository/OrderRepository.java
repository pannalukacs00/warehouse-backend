package ubb.hpc.fmcp.transactionalsystem.repository;

import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ubb.hpc.fmcp.transactionalsystem.exception.EntityNotFoundException;
import ubb.hpc.fmcp.transactionalsystem.model.Order;
import ubb.hpc.fmcp.transactionalsystem.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
public class OrderRepository {
    @Autowired
    private HikariDataSource dataSource;

    @Autowired
    private ProductRepository productRepository;

    public List<Order> findAll() throws SQLException {
        String sqlStatement = "SELECT * FROM Orders;";
        List<Order> orderList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    orderList.add(resultSetToOrder(resultSet));
                }
                connection.commit();
                return orderList;
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } finally {
                if (connection != null)
                    connection.setAutoCommit(true);
            }
        }

        return Collections.emptyList();
    }

    public Optional<Order> findById(Long id) throws SQLException {
        String sqlStatement = "SELECT * FROM orders WHERE id = ?";

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setLong(1, id);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    connection.commit();
                    return resultSetToOptionalOrder(resultSet);
                } else {
                    throw new EntityNotFoundException(id);
                }
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } catch (EntityNotFoundException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
                throw e;
            } finally {
                connection.setAutoCommit(true);
            }
            return Optional.empty();
        }
    }

    public Order save(Order order) throws SQLException {
        String sqlStatement;
        boolean isUpdating = false;

        if (order.getId() == null) {
            sqlStatement = "INSERT INTO orders (completed, quantity, product_id, ordered_on) VALUES (?, ?, ?, ?)";
        } else {
            isUpdating = true;
            sqlStatement = "UPDATE orders SET completed=?, quantity=?, product_id=?, ordered_on=? WHERE id=?";
        }

        try (Connection connection = dataSource.getConnection()) {

            connection.setAutoCommit(false);

            try (PreparedStatement statement = connection.prepareStatement(sqlStatement)) {
                statement.setBoolean(1, order.getCompleted());
                statement.setInt(2, order.getQuantity());
                statement.setLong(3, order.getProduct().getId());
                statement.setDate(4, java.sql.Date.valueOf(order.getOrderedOn().toString()));

                if (Boolean.TRUE.equals(isUpdating)) {
                    statement.setLong(5, order.getId());
                }
                statement.executeUpdate();
                connection.commit();
                return order;
            } catch (SQLException e) {
                if (connection != null) {
                    log.error("Transaction is being rolled back!");
                    connection.rollback();
                }
            } finally {
                connection.setAutoCommit(true);
            }
        }
        return null;
    }

    private Optional<Order> resultSetToOptionalOrder(ResultSet resultSet) throws SQLException, NullPointerException {
        Product product = productRepository.findById(resultSet.getLong(5)).get();

        Order order = new Order();
        order.setId(resultSet.getLong(1));
        order.setCompleted(resultSet.getBoolean(2));
        order.setOrderedOn(resultSet.getDate(3).toLocalDate());
        order.setQuantity(resultSet.getInt(4));
        order.setProduct(product);

        return Optional.ofNullable(order);
    }

    private Order resultSetToOrder(ResultSet resultSet) throws SQLException, NullPointerException {
        Product product = productRepository.findById(resultSet.getLong(5)).get();

        Order order = new Order();
        order.setId(resultSet.getLong(1));
        order.setCompleted(resultSet.getBoolean(2));
        order.setOrderedOn(resultSet.getDate(3).toLocalDate());
        order.setQuantity(resultSet.getInt(4));
        order.setProduct(product);

        return order;
    }

}