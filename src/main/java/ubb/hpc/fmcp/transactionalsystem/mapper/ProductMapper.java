package ubb.hpc.fmcp.transactionalsystem.mapper;

import org.springframework.stereotype.Component;
import ubb.hpc.fmcp.transactionalsystem.dto.ProductDTO;
import ubb.hpc.fmcp.transactionalsystem.model.Product;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductMapper {
    public static ProductDTO modelToDto(Product product) {
        return new ProductDTO(
                product.getId(),
                product.getName(),
                product.getQuantity(),
                product.getImporter().getName()
        );
    }

    public static List<ProductDTO> modelsToDtos(List<Product> products) {
        List<ProductDTO> list = new ArrayList<>(products.size());
        for (Product product : products) {
            list.add(modelToDto(product));
        }
        return list;
    }
}
