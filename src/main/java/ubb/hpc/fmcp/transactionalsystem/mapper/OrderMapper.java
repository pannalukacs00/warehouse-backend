package ubb.hpc.fmcp.transactionalsystem.mapper;

import ubb.hpc.fmcp.transactionalsystem.dto.OrderDTO;
import ubb.hpc.fmcp.transactionalsystem.model.Order;
import ubb.hpc.fmcp.transactionalsystem.model.Product;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderMapper {
    public static OrderDTO modelToDto(Order order) {
        return new OrderDTO(
                order.getId(),
                order.getProduct().getName(),
                order.getQuantity(),
                order.getOrderedOn().toString(),
                order.getCompleted()
        );
    }

    public static List<OrderDTO> modelsToDtos(List<Order> orders) {
        List<OrderDTO> list = new ArrayList<>(orders.size());
        for (Order order : orders) {
            list.add(modelToDto(order));
        }
        return list;
    }

    public static Order dtoToModel(OrderDTO orderDTO, Product product) {
        return new Order(null, product, LocalDate.parse(orderDTO.getDate()), orderDTO.getQuantity(), false);
    }
}
