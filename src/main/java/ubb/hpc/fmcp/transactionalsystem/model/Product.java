package ubb.hpc.fmcp.transactionalsystem.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;
    private Integer quantity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    @ToString.Exclude
    private Contractor importer;

    @OneToMany(
            mappedBy = "product",
            cascade = CascadeType.ALL,

            orphanRemoval = true
    )
    @JsonIgnore
    @ToString.Exclude
    private List<Order> orderList = new ArrayList<>();

}
