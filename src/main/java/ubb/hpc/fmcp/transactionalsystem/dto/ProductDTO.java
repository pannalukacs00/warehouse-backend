package ubb.hpc.fmcp.transactionalsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductDTO {
    private Long id;
    private String name;
    private Integer quantity;
    private String contractor;
}
