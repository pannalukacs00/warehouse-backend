package ubb.hpc.fmcp.transactionalsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderDTO {
    private Long id;
    private String product;
    private Integer quantity;
    private String date;
    private Boolean completed;
}
